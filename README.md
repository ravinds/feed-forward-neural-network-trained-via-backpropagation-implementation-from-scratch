# Feed Forward Neural Network via Backpropagation - Implementation from Scratch

**Summary:** 

The neural network algorithm trained by the process of backpropagation is implemented. The stochastic gradient descent (SGD) is adopted for this algorithm i.e., the weight and bias matrices are updated after learning from the errors of each dataset point one by one. In this assignment, 3 layer (input, one hidden and output) neural network is considered. The mathematical equation involved are given below,

*FeedForward:*

[H] = sigma([W_IH].[I] + [B_H])

[O] = sigma([W_HO].[H] + [B_O])

*Backpropagation:*

[delta_W_HO] = (learning_rate)[Output_Errors]x[O(1-O)].[H_tranpose]

[delta_W_IH] = (learning_rate)[Hiddden_Errors]x[H(1-H)].[I_tranpose]

[delta_B_O] = (learning_rate)[Output_Errors]x[O(1-O)]

[delta_B_H] = (learning_rate)[Hidden_Errors]x[H(1-H)]

Where, [] represents the matrix, x represents the Hadamard multiplication (elementwise), and . represents the usual matrix multiplication.

H represents hidden matrix, O represents output matrix, W_IH represents weight matrix between input layer and hidden layer, W_HO represents weight matrix between hidden layer and output layer, B_H represents bias matrix for hidden layer, B_O represents bias matrix for output layer.

The chosen activation function is Sigmoid, and the derivative of Sigmoid is function sigma(x)[1 - sigma(x)].


**Results:**

I decided to keep 2 nodes in my hidden layer for this analysis, and a learning rate of 0.1.

The comparative analysis is done between my algorithm and sklearn, and it has been depicted through the following two tables,

|Accuracy Comparison|XOR|AND|OR|
| --- | --- | --- | --- |
|My algorithm|0.5|0.25|0.75|
|Sklearn|0.5|0.75|0.75|


|Computation time Comparison|XOR|AND|OR| 
| --- | --- | --- | --- |
|My algorithm|0.002313|0.001074|0.002284|
|Sklearn|0.004234|0.0040423|0.00698|

In terms of accuracy, the sklearn algorithm performed better for AND problem than my algorithm, however both algorithm performed equally well for XOR and OR problems.

In terms of computation time,  my algorithm performed much better than sklearn across all three problems.


**Reflection:**

*Useful takeaways:*

The multilayered neural networks are very powerful algorithms with each additional layer increasing its learning power. However, I am not sure if increasing the number of nodes directly relates to the learning power of the neural network.

*Things that surprised me:*

The backpropagation at first seems to be difficult concept, however after closely working through the algorithm, I understood that it is simply a more structured form of gradient descent in linear regression, with one additional thing to take care i.e., to include the gradient of the activation function that was missing in case of linear regression.

*Things that caused some difficulty:*

I could not understand the trend between the number of nodes in hidden layer and the accuracy of the model. With increasing number of nodes in hidden layer, the bias matrices failed to have a substantial change and remained constant, which resulted in overall low accuracy (on training dataset itself). This was surprising too.





