#!/usr/bin/env python
# coding: utf-8

# In[ ]:

import numpy as np
import math

def Matrix(nrows, ncols):
    matrix = []
    for i in range(0,nrows):
        matrix.append([])
        for j in range(0,ncols):
            matrix[i].append(5)
    return matrix
        
def MatrixScalarMultiply(n,matrix):
    for i in range(0,len(matrix)):
        for j in range(0,len(matrix[i])):
            matrix[i][j] = matrix[i][j]*n
    
    return matrix

def MatrixScalarAdd(n,matrix):
    for i in range(0,len(matrix)):
        for j in range(0,len(matrix[i])):
            matrix[i][j] = matrix[i][j] + n
    
    return matrix

def MatrixAdd(matrix1, matrix2):
    if len(matrix1) == len(matrix2) & len(matrix1[0]) == len(matrix2[0]):
        for i in range(0,len(matrix1)):
            for j in range(0,len(matrix1[i])):
                matrix1[i][j] = matrix1[i][j] + matrix2[i][j]
    return matrix1


def MatrixSubtract(matrix1, matrix2):
    for i in range(0,len(matrix1)):
        for j in range(0,len(matrix1[i])):
            matrix1[i][j] = matrix1[i][j] - matrix2[i][j]
    return matrix1


def MatrixProduct(matrix1,matrix2):
    #if len(matrix1[0]) == len(matrix2):
    productmatrix = Matrix(len(matrix1), len(matrix2[0])) # Creating a matrix with all zero entries
    for i in range(0,len(productmatrix)):
        for j in range(0,len(productmatrix[0])):
            for k in range(0,len(matrix1[0])):
                productmatrix[i][j] = productmatrix[i][j] + matrix1[i][k]*matrix2[k][j]
    return productmatrix 

def HadamardProduct(matrix1,matrix2):
    productmatrix = Matrix(len(matrix1), len(matrix1[0])) # Creating a matrix with all zero entries
    for i in range(0,len(productmatrix)):
        for j in range(0,len(productmatrix[0])):
            productmatrix[i][j] = matrix1[i][j]*matrix2[i][j]
    return productmatrix


def MatrixTranspose(matrix):
    transposematrix = Matrix(len(matrix[0]), len(matrix))
    for i in range(0,len(matrix)):
        for j in range(0,len(matrix[0])):
            transposematrix[j][i] = matrix[i][j]
    return transposematrix

def Map(func,matrix):
    outputmatrix = Matrix(len(matrix),len(matrix[0]))
    for i in range(0,len(matrix)):
        for j in range(0,len(matrix[i])):
            outputmatrix[i][j] = func(matrix[i][j])
                                      
    return outputmatrix
    
def Sigmoid(x):
    return 1/(1+math.exp(-x))


def ReLU(x):
    if (x <= 0):
        return 0
    else:
        return x


def DerivativeSigmoid(y):
    return y*(1-y)

    
def FeedForward(X,model):
    guess = []
    for i in range(0,len(X)):
        guess.append(0)
    for i in range(0,len(X)):
        hidden_matrix = Map(Sigmoid,MatrixAdd(MatrixProduct(model[0],X[i]),model[2]))
        guess[i] = Map(Sigmoid,MatrixAdd(MatrixProduct(model[1],hidden_matrix),model[3]))
    return guess
    
                                                        
def BackpropagationOneStep(i,h,o,learning_rate,input_matrix,target_matrix):
    W_IH = Matrix(h,i)
    W_HO = Matrix(o,h)
    B_H = Matrix(h,1)
    B_O = Matrix(o,1)
    
    input_matrix_transpose = MatrixTranspose(input_matrix)
    hidden_matrix = Map(Sigmoid,MatrixAdd(MatrixProduct(W_IH,input_matrix),B_H))
    hidden_matrix_transpose = MatrixTranspose(hidden_matrix)
    output_matrix = Map(Sigmoid,MatrixAdd(MatrixProduct(W_HO,hidden_matrix),B_O))
        
    output_error = MatrixSubtract(target_matrix,output_matrix)
    
    gradient_HO = MatrixScalarMultiply(learning_rate,HadamardProduct(output_error,Map(DerivativeSigmoid,output_matrix)))
    Weight_HO_deltas = MatrixProduct(gradient_HO,hidden_matrix_transpose)
    
    W_HO = MatrixAdd(W_HO,Weight_HO_deltas)
    B_O = MatrixAdd(B_O,gradient_HO)
    
    hidden_error = MatrixProduct(MatrixTranspose(W_HO),output_error)
                                 
    gradient_IH = MatrixScalarMultiply(learning_rate,HadamardProduct(hidden_error,Map(DerivativeSigmoid,hidden_matrix)))
    Weight_IH_deltas = MatrixProduct(gradient_IH,input_matrix_transpose)
    
    W_IH = MatrixAdd(W_IH,Weight_IH_deltas)
    B_H = MatrixAdd(B_H,gradient_IH)
    
    return [W_IH,W_HO,B_H,B_O]
    
    
def BackpropagationTraining(i,h,o,learning_rate,X,Y):
    W_IH = Matrix(h,i)
    W_HO = Matrix(o,h)
    B_H = Matrix(h,1)
    B_O = Matrix(o,1)
    
    for i in range(0,len(X)):
        input_matrix_transpose = MatrixTranspose(X[i])
        hidden_matrix = Map(Sigmoid,MatrixAdd(MatrixProduct(W_IH,X[i]),B_H))
        hidden_matrix_transpose = MatrixTranspose(hidden_matrix)
        
        output_matrix = Map(Sigmoid,MatrixAdd(MatrixProduct(W_HO,hidden_matrix),B_O))
        output_error = MatrixSubtract(Y[i],output_matrix)
        
        hidden_error = MatrixProduct(MatrixTranspose(W_HO),output_error)
        
        gradient_HO = MatrixScalarMultiply(learning_rate,HadamardProduct(output_error,Map(DerivativeSigmoid,output_matrix)))
        Weight_HO_deltas = MatrixProduct(gradient_HO,hidden_matrix_transpose)

        W_HO = MatrixAdd(W_HO,Weight_HO_deltas)
        B_O = MatrixAdd(B_O,gradient_HO)

        gradient_IH = MatrixScalarMultiply(learning_rate,HadamardProduct(hidden_error,Map(DerivativeSigmoid,hidden_matrix)))
        Weight_IH_deltas = MatrixProduct(gradient_IH,input_matrix_transpose)

        W_IH = MatrixAdd(W_IH,Weight_IH_deltas)
        B_H = MatrixAdd(B_H,gradient_IH)
    
    return [W_IH,W_HO,B_H,B_O]
    
    
    


    
    
    
    
    
    
    
    
    



















